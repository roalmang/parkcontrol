/***************************
  Name:		ParkControl.ino
 Created:	27.12.2017 17:06:36
 Author:	robert
***************************/

#include <UTFT.h>
#include <memorysaver.h>
#include <SoftwareSerial.h>
#include <HardwareSerial.h>
#include <avr\pgmspace.h>
#include "UTFT/UTFT.cpp"
#include "UTFT/DefaultFonts.c"
#include <Wire.h>

extern unsigned short car[];

#define SIM800_TX_PIN	13	//SIM800 TX is connected to Arduino D8
#define SIM800_RX_PIN	12	//SIM800 RX is connected to Arduino D7
#define SIM800_NRES_PIN 18
#define STD_TIMEOUT		2		// seconds
#define NUM_SENSORS		4
#define BUF_SIZE		100

SoftwareSerial simSerial(SIM800_TX_PIN, SIM800_RX_PIN);    //Create software serial object to communicate with SIM800
UTFT myGLCD(SSD1289, 38, 39, 40, 41);

const char serverAdr[] = "qdm73tfyzfoiclo8.myfritz.net";
const char serverPort[] = "52222";
const char dataFormat[] = "-%i-%i-%i-%i";
const char errResp[] = "ERROR\r\n";
char valueArray[BUF_SIZE];

int errRespLen = strlen(errResp);
int trigger[4] = { 9,8,11,10 };			// VR, VL, HR, HL Pinkonfiguration beachten
int echo[4] = { 15,14,17,16 };			// VR, VL, HR, HL Pinkonfiguration beachten
int displayXCoordinates[4] = { 50, 50, 230, 230 };// VR, VL, HR, HL  
int displayYCoordinates[4] = { 50, 175, 50, 175 };// VR, VL, HR, HL 
int distance[NUM_SENSORS];
int picStartX = 80;
int picStartY = 80;
int picSizeX = 140;
int picSizeY = 72;
const int MPU_addr=0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

bool crash_detected = false;


int simSerialInOut(const char *cmd, const char *expResp, unsigned int timeoutSec);
void readSensorData(void);
void drawSensorData(void);
void printSensorData(void);
int sendData(const char *serverName, const char *port, const char *data);
void read_acc_data(void);
int initWireless(void);
int setupWireless(void);
int checkIp(void);
int checkPin(void);
int simInit(void);
void serialDebug(void);

void setup()
{
  //Begin serial comunication with Arduino and Arduino IDE (Serial Monitor)
  Serial.begin(9600);
  while (!Serial);

  simSerial.begin(9600);
  delay(1000);

  /* Disable unused serial interfaces */
  Serial2.end();
  Serial3.end();

  //Set pinmode
  pinMode(trigger[0], OUTPUT);
  pinMode(trigger[1], OUTPUT);
  pinMode(trigger[2], OUTPUT);
  pinMode(trigger[3], OUTPUT);
  pinMode(echo[0], INPUT);
  pinMode(echo[1], INPUT);
  pinMode(echo[2], INPUT);
  pinMode(echo[3], INPUT);

  pinMode(SIM800_NRES_PIN, OUTPUT);
  digitalWrite(SIM800_NRES_PIN, HIGH);

  // Setup the LCD 
  // randomSeed(analogRead(0));

    myGLCD.InitLCD();
    myGLCD.setFont(BigFont);
    myGLCD.setBackColor(VGA_BLACK);
    myGLCD.drawBitmap(picStartX, picStartY, picSizeX, picSizeY, car, 1);

    Wire.begin();
    Wire.beginTransmission(MPU_addr);
    Wire.write(0x6B);  // PWR_MGMT_1 register
    Wire.write(0);     // set to zero (wakes up the MPU-6050)
    Wire.endTransmission(1); // Wire.endTransmission(true);

    simInit();
    initWireless();

    Serial.println("Setup Complete!");
}

void loop() {
	if (crash_detected)
	{
		readSensorData();
		drawSensorData();
		printSensorData();
		sendData(serverAdr, serverPort, valueArray);
		delay(1000);
	}
}


int simSerialInOut(const char *cmd, const char *expResp, unsigned int timeoutSec) {
  char c = 0;
  int expRespLen = strlen(expResp);
  int sumExp = 0, sumErr = 0;
  unsigned long t1, t2;

  Serial.print("Sending: ");
  Serial.println(cmd);

  simSerial.write(cmd);

  Serial.print("[");

  t1 = millis();

  while (sumExp != expRespLen) {
    if (simSerial.available()) {
      c = simSerial.read();
      if (c > 32 && c < 127) {
        Serial.print(c);
      }
      else {
        Serial.print(".");
      }

      if (c == expResp[sumExp]) sumExp++;
      if (c == errResp[sumErr]) sumErr++;
      if (sumErr == errRespLen) return -1;

    }

    t2 = millis();

    if ((t2 - t1) > (timeoutSec * 1000)) {
      Serial.println("] - Timeout!");
      return 1;
    }
  }

  Serial.println("]");

  return 0;
}

void readSensorData() {
  for (int i = 0; i < NUM_SENSORS; i++) {
    int value = 0;
    int time = 0;

    digitalWrite(trigger[i], LOW); // Setze Spannung an Trigger auf 0 zur Rauschunterdr�ckun
    delayMicroseconds(10);
    digitalWrite(trigger[i], HIGH); // Aktor starten, Ton fuer 10 m
    delayMicroseconds(10);

    digitalWrite(trigger[i], LOW); // Setze Spannung an Trigger auf 0 zur Rauschunterdr�ckun    
    delayMicroseconds(10);
    digitalWrite(trigger[i], HIGH); // Aktor starten, Ton fuer 10 m   
    delayMicroseconds(10);
    digitalWrite(trigger[i], LOW);

    time = pulseIn(echo[i], HIGH);

    value = ((float)time * 0.03432) / 2.0;

    if (value >= 500) { value = -1; }  // Abfangen von Messfehlern

    distance[i] = value;
  }
  delay(200);
}


void drawSensorData()
{
  // Screen leeren
  myGLCD.clrScr();

  // Kleinsten gemessenen Abstand bestimmen
  int minValue = distance[0];
  for (int i = 1; i < NUM_SENSORS; i++) {
    if (distance[i] < minValue) minValue = distance[i];
  }

  // Zuweisung der Hintergrundfarbe in Abhängigkeit des minimalen gemessenen Abstands
  if (minValue <= 30)  myGLCD.fillScr(VGA_RED);
  else if (minValue> 30 && minValue <= 80) myGLCD.fillScr(VGA_YELLOW);
  else myGLCD.fillScr(VGA_GREEN);

  // Ausgabe der Sensorwerte zusammen mit dem Piktogramm des Autos auf das Display
  myGLCD.drawBitmap(picStartX, picStartY, picSizeX, picSizeY, car);
  for (int i = 0; i < NUM_SENSORS; ++i) {
    myGLCD.printNumI(distance[i], displayXCoordinates[i], displayYCoordinates[i]);
  }

}

void printSensorData() {
  /****************************************************
  * Erzeugt eines leeren Strings um dann die Sensordaten darin zu schreiben
  * Zum versenden muss der String dann in ein Char Array umgewandelt werden
  */
  String telegram = "-";
  for (int i = 0; i < NUM_SENSORS; i++) {
    //Serial.print(distance[i]);
    //Serial.print(", ");
    if (i == NUM_SENSORS - 1) telegram = telegram + distance[i] + "\r\n";
    else telegram = telegram + distance[i] + "-";
  }
  telegram.toCharArray(valueArray, BUF_SIZE);
}

int sendData(const char *serverName, const char *port, const char *data) {
  /*  Open Socket  */
  char cipStart[] = "AT+CIPSTART=\"UDP\",\"%s\",\"%s\"\r\n";
  char cipMsg[80];
  int ret = -1;

  Serial.print("Connecting to ");
  Serial.print(serverName);
  Serial.print(":");
  Serial.println(port);

  snprintf(cipMsg, sizeof(cipMsg), cipStart, serverName, port);

  ret = simSerialInOut(cipMsg, "CONNECT OK\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CIPSEND\r\n", "AT+CIPSEND\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  simSerial.write(data);
  ret = simSerialInOut("\x1A", "SEND OK\r\n", 5);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CIPSHUT\r\n", "SHUT OK\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  return 0;
}

void read_acc_data(void){
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  
  Serial.print("AcX = "); Serial.print(AcX);
  Serial.print(" | AcY = "); Serial.print(AcY);
  Serial.print(" | AcZ = "); Serial.print(AcZ);
  Serial.print(" | Tmp = "); Serial.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
  delay(333);
}

int initWireless() {
  int ret = 0, i = 0;

  for (i = 0; i < 5; i++) {
    ret = setupWireless();

    if (ret == 0) break;

    if (i == 4) {
      Serial.println("initWireless failed. Running serialDebug");
      serialDebug();
    }
  }

  return 0;
}

int setupWireless() {
  int ret;
  const char csttCommand[] = "AT+CSTT=\"internet.telekom\",\"congstar\",\"cs\"\r\n";

  ret = simSerialInOut("AT+CGATT?\r\n", "+CGATT:1\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CIPSHUT\r\n", "SHUT OK\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CIPSTATUS\r\n", "STATE: IP INITIAL\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut(csttCommand, "OK\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CIICR\r\n", "OK\r\n", STD_TIMEOUT);
  Serial.println("5 sec CIICR delay...");
  delay(5000);

  ret = simSerialInOut("AT+CIPSTATUS\r\n", "STATE: IP GPRSACT\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = checkIp();
  if (ret < 0) return -1;

  return 0;
}

int checkIp() {
  char c = 0;
  unsigned long t1, t2;
  int sumErr = 0;

  Serial.print("Sending: AT+CIFSR\r\n");
  simSerial.write("AT+CIFSR\r\n");
  Serial.print("[");

  t1 = millis();

  while (true) {
    c = simSerial.read();
    if (c > 31 && c < 127) {
      Serial.print(c);
    }

    if (c == errResp[sumErr]) sumErr++;
    if (sumErr == errRespLen) {
      Serial.print("]");
      return -1;
    }

    t2 = millis();

    if ((t2 - t1) > (2000)) {
      break;
    }
  }

  Serial.print("]");

  return 0;
}


int checkPin() {
  int ret = 0;

  Serial.println("Resetting SIM800");
  digitalWrite(SIM800_NRES_PIN, LOW);
  delay(1000);
  digitalWrite(SIM800_NRES_PIN, HIGH);
  delay(2000);

  ret = simSerialInOut("AT\r\n", "OK\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  ret = simSerialInOut("AT+CPIN?\r\n", "+CPIN: READY\r\n", STD_TIMEOUT);
  if (ret < 0) return -1;

  Serial.println("SIM init delay ...");
  delay(2000);

  return 0;
}


int simInit() {
  int ret = 0, i = 0;

  for (i = 0; i < 5; i++) {
    ret = checkPin();

    if (ret == 0) break;

    if (i == 4) {
      Serial.println("simInit failed. Running serialDebug");
      serialDebug();
    }
  }

  return 0;
}


void serialDebug(void) {
  while (1) {
    //Read SIM800 output (if available) and print it in Arduino IDE Serial Monitor
    if (simSerial.available()) {
      Serial.write(simSerial.read());
    }
    //Read Arduino IDE Serial Monitor inputs (if available) and send them to SIM800
    if (Serial.available()) {
      simSerial.write(Serial.read());
    }
  }
}
